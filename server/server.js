var http = require('http');
var url = require('url');
var querystring = require('querystring');
var static = require('node-static');
var file = new static.Server('.', { cache: 0 });

function accept(req, res) {

  var urlParsed = url.parse(req.url, true);
  switch (urlParsed.pathname) {
    case '/api/comments':
      cgetComments (req, res);
      break;
    default:
      file.serve(req, res);
  }
}

// ------ запустить сервер ---------

if (!module.parent) {
  http.createServer(accept).listen(8081);
} else {
  exports.accept = accept;
}

function cgetComments(req, res) {
  res.setHeader('Content-Type', 'application/javascript; charset=utf-8');
  var comments = [
    {"id": "1", "author": "Pete Hunt", "text": "This is one comment"},
    {"id": "2", "author": "Jordan Walke", "text": "This is *another* comment"}
  ];

  res.writeHead(200, {'Access-Control-Allow-Origin' : '*'});
  res.end(JSON.stringify(comments));
}
