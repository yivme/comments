import {EventEmitter} from 'events';

import Dispatcher from '../dispatcher/dispatcher';
import ConstantsComments from '../constants/comments';
import CommentsAPI from '../utils/commentsAPI';

const CHANGE_EVENT = 'change';

const newComment = { author: '', text: '' };

let
  defineKey = (items) => {
    const ids = items.map((item) => { return item.id });
    return Math.max.apply(null, ids) + 1;
  };

CommentsAPI.get();

class Comment extends EventEmitter {

    _store = {
      newComment: {...newComment},
      comments: [],
      pending: false
    };

    addChangeListener = (cb) => {
        this.on(CHANGE_EVENT, cb);
    };

    removeChangeListener = (cb) => {
        this.removeListener(CHANGE_EVENT, cb);
    };

    getComment = () => {
        return this._store;
    };

    createNewItem = data => {
        const comments = this._store.comments;
        this._store.newComment = {...newComment};
        comments.push({ ...data, id: defineKey(comments) });
        this._store = Object.assign(this._store, { comments: comments });
        this.emit(CHANGE_EVENT);
    };

    receiveItems = data => {
        this._store = { ...this._store, pending: false, comments: data };
        this.emit(CHANGE_EVENT);
    };

    removeItems = id => {
        const index = this._store.comments.findIndex((item) => item.id === id );
        this._store.comments.splice(index, 1);
        this.emit(CHANGE_EVENT);
    }
}

const comment = new Comment();

Dispatcher.register((action) => {
    switch (action.actionType) {
        case ConstantsComments.NEW_ITEM:
            comment.createNewItem(action.data);
            break;
        case ConstantsComments.GET_ITEMS:
            break;
        case ConstantsComments.RECEIVE_ITEMS:
            comment.receiveItems(action.data);
            break;
        case ConstantsComments.REMOVE_ITEM:
            comment.removeItems(action.id);
            break;
        default:
            return true;
    }
});

export default comment;