jest.dontMock('../../constants/comments.js');
jest.dontMock('../comments.js');
jest.dontMock('object-assign');


//import CommentsStore from '../comments';
describe('CommentsStore', function() {

  const CommentsConstants = require('../../constants/comments').default;
  let AppDispatcher,
    CommentsStore,
    callback;

  // mock actions
  let actionCommentCreate = {
    actionType: CommentsConstants.NEW_ITEM,
    data: {
      author: 'test author',
      text: 'test text'
    }
  };

  beforeEach(function() {
    AppDispatcher = require('../../dispatcher/dispatcher').default;
    CommentsStore = require('../comments').default;
    callback = AppDispatcher.register.mock.calls[0][0];
  });

  it('registers a callback with the dispatcher', function() {
    expect(AppDispatcher.register.mock.calls.length).toBe(1);
  });

  it('Проверяем начальное состояние', function() {
    var all = CommentsStore.getComment().comments;
    expect(all.length).toBe(0);
  });

  it('Создаем коммент', function() {
    callback(actionCommentCreate);
    var all = CommentsStore.getComment().comments;
    expect(all.length).toBe(1);
  });

  it('Создаем коммент', function() {
    callback(actionCommentCreate);
    var all = CommentsStore.getComment().comments;
    expect(all.length).toBe(1);
  });

  it('Удаляем коммент', function() {
    callback(actionCommentCreate);
    var all = CommentsStore.getComment().comments;
    expect(all.length).toBe(1);
  });

});