import React, { Component, PropTypes as pt } from 'react';

import CommentForm from './comment/commentForm';
import CommentList from './comment/commentList';
import CommentStore from '../stores/comments';
import Banner from './ui/banner/banner.jsx'

require('./main.less');

class Content extends Component {

    componentWillMount() {
        this.setState(CommentStore.getComment());
    }

    componentDidMount() {
        CommentStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        CommentStore.removeChangeListener(this._onChange);
    }

    _onChange = () => {
        this.setState(CommentStore.getComment());
    };

    render() {

        return <main className="content">
            <Banner />
            <CommentForm data={this.state.newComment}/>
            <CommentList data={this.state}/>
        </main>
    }
}

export default Content;