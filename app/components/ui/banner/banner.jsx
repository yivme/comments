import React, { Component, PropTypes as pt } from 'react';

import './banner.less';

const items = [0,1,2];

class Banner extends Component {

  state = {index: 0};

  click = e => {
    const t = (e.target);
    const next = parseInt(t.dataset.i);

    this.setState({index: next});
  };

  bg = () => {
    return items.map((item)=>{
      let cn = `b-bg b-bg-${item}`;

      if (this.state.index === item) {
        cn = cn + ' b-bg-show';
      } else {
        cn = cn + ' b-bg-hide';
      }
      return <div className={cn}></div>;
    });
  };

  render() {
    return <div className='b'>
      {this.bg()}

      <a className="b-bt-0" data-i="0" onClick={this.click}>click</a>
      <a className="b-bt-1" data-i="1" onClick={this.click}>click</a>
      <a className="b-bt-2" data-i="2" onClick={this.click}>click</a>

    </div>
  }
}

export default Banner;