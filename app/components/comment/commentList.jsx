import React, { Component, PropTypes as pt } from 'react';

import Comment from './comment.jsx';

class CommentList extends Component {

  render() {

    let commentNodes;

    if (this.props.data.pending) {
      commentNodes = 'pending ... ';
    } else {
      commentNodes = this.props.data.comments.map((comment) => {
        return (
          <Comment author={comment.author} key={comment.id}>
            {comment.text}
          </Comment>
        );
      });
    }

    return <div className="comment-list">
      {commentNodes}
    </div>
  }
}

export default CommentList;