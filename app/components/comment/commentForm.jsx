import React, { Component, PropTypes as pt } from 'react';

import ActionsComments from '../../actions/comments';

import './commentForm.less';

class CommentForm extends Component {

    componentWillMount() {
        const { author, text } = this.props.data;

        this.setState({ author, text });
    }

    componentWillReceiveProps(nextProps) {
        const { author, text } = nextProps.data;

        this.setState({ author, text });
    }

    handleChange = event => {
        const { value, name } = event.target;
        this.setState(Object.assign(this.state, {[name]: value}));
    };

    handleClick = ev => {
        ev.preventDefault();
        ActionsComments.addComment(this.state);
    };

    render() {

        return <div className="commentForm">
            <form action="">
                <label className="comment-form__label" for="author">Автор:</label>
                <input
                    name="author"
                    className="comment-form__input"
                    hintText="Name"
                    value={this.state.author}
                    onChange={this.handleChange}
                />
                <br />
                <label className="comment-form__label" for="author">Текст:</label>
                <textarea
                    name="text"
                    className="comment-form__textarea"
                    value={this.state.text}
                    onChange={this.handleChange}
                />
                <br />
                <button onClick={this.handleClick} value='add' />
            </form>
        </div>
    }
}

export default CommentForm;