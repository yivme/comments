import React, { Component, PropTypes as pt } from 'react';

import ActionsComments from '../../actions/comments';

import './comment.less';

class Comment extends Component {
  render() {
    return <div className="comment">
      <h2 className="commentAuthor">
        {this.props.author}
      </h2>
      {this.props.children}
      <div
        className="comment__remove"
        onClick={this._clickRemove}
      >x</div>
    </div>
  }

  _clickRemove = () => {
    ActionsComments.removeComment(this.props.key);
  }

}

export default Comment;