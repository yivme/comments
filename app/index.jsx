import React from 'react';
import ReactDOM from 'react-dom';

import Header from './components/header.jsx';
import Main from './components/main.jsx';
import Footer from './components/footer.jsx';

import './index.less';

ReactDOM.render(
  <div className="container">
    <Header />
    <Main />
    <Footer />
  </div>,
  document.getElementById('app')
);