import Dispatcher from '../dispatcher/dispatcher';
import Constants from '../constants/comments';
import CommentsAPI from '../utils/commentsAPI';

export default {

    addComment(data) {
        Dispatcher.dispatch({
            actionType: Constants.NEW_ITEM,
            data: data
        });
    },

    getComments() {
        Dispatcher.dispatch({
            actionType: Constants.GET_ITEMS
        });

        CommentsAPI.get();
    },

    removeComment(id) {
      Dispatcher.dispatch({
        actionType: Constants.REMOVE_ITEM,
        id: id
      });
    },

    receiveComments(data) {
        Dispatcher.dispatch({
            actionType: Constants.RECEIVE_ITEMS,
            data: data
        });
    }

};