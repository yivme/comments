import ActionsComments from '../actions/comments';
import request from 'superagent';

export default {

  get () {
    request.get('http://localhost:8081/api/comments')
      .set('Accept', 'application/json')
      .end(function(err, response) {
        if (err) return console.error(err);

        const comments = JSON.parse(response.text);

        ActionsComments.receiveComments(comments);
      });
  }
};